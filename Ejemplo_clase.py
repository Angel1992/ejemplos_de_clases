#Trabajo Grupal Clases
'''INTEGRANTES
*Steven Velez
*Angel Contento
*Juan Alvarez'''

class Rectangulo:
    """
    Tema Área de paralelogramos y trapecios
     (Formula para calcular el área de un rectángulo)
    """
    def _init_(self, b, h):
        self.b = b
        self.h = h

    def area(self):
        return self.b * self.h

rectangulo = Rectangulo(20, 10)
print("Área del rectángulo: ", rectangulo.area())

"""
  Tema Plano cartesiano
   (Puntos en los cuadrantes del plano cartesiano)
  """

class Punto:

    def _init_(self,x,y):
        self.x=x
        self.y=y

    def imprimir(self):
        print("Coordenada del punto")
        print("(",self.x,",",self.y,")")

        def imprimir_cuadrante(self):
            if self.x > 0 and self.y > 0:
                print("Primer cuadrange")
            else:
                if self.x < 0 and self.y > 0:
                    print("Segundo cuadrante")
                else:
                    if self.x < 0 and self.y < 0:
                        print("Tercer cuadrante")
                    else:
                        if self.x > 0 and self.y < 0:
                            print("Cuarto cuadrante")

    punto1 = Punto(10, -2)
    punto1.imprimir()
    punto1.imprimir_cuadrante()